<?php

namespace Tui\StringBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Tui\StringBundle\Entity\TuiString;
use Tui\StringBundle\TuiStringSerializer;
use Tui\StringBundle\Repository\TuiStringRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class StringController extends AbstractController
{
    public const DEFAULT_FORMAT = 'i18n';

    #[Route('/strings/{path}', name: 'tui_string_get', methods: ['GET'])]
    public function find(
        Request $request,
        TuiStringRepository $repository,
        TuiStringSerializer $stringSerializer,
        TagAwareCacheInterface $tuiStringCachePool,
        string $path
    ): JsonResponse
    {
        $format = $this->getOutputFormat($request);
        $path = $this->cleanPath($path);

        $data = $tuiStringCachePool->get($format . '.' . $path, function (ItemInterface $item) use ($path, $repository, $stringSerializer, $format) {
            $entities = $repository->findLikePath($path);
            $data = $stringSerializer->encodeEntities($entities);

            if ($format === 'i18n') {
                $data = $stringSerializer->toI18n($data);
            }

            $item->tag($stringSerializer->getCacheKeys($path));
            return $data;
        });

        return $this->json($data);
    }

    #[Route('/strings/{path}', name: 'tui_string_set', methods: ['PUT'])]
    public function update(
        Request $request,
        TuiStringRepository $repository,
        TuiStringSerializer $stringSerializer,
        ValidatorInterface $validator,
        TagAwareCacheInterface $tuiStringCachePool,
        string $path
    ): JsonResponse
    {
        $path = $this->cleanPath($path);

        // Turn request into an array of path:data strings, grouped by locale
        try {
            $parsedData = json_decode($request->getContent(), true);

            if (!is_array($parsedData)) {
                throw new \Exception('Expected valid JSON object');
            }

            $parsedData = $stringSerializer->fromI18n($parsedData);

            // Validate the basic shape
            foreach ($parsedData as $locale => $messages) {
                if (!is_array($messages)) {
                    throw new \Exception('Invalid data format');
                }
                foreach ($messages as $key => $message) {
                    $cleanKey = $this->cleanPath($key);
                    if ($cleanKey !== $key) {
                        throw new \Exception(sprintf('Invalid message path in data: %s', $key));
                    }
                    if (strpos($key, $path) !== 0) {
                        throw new \Exception('One or more message paths are outside the scope of the URL path');
                    }
                }
            }
        } catch (\Exception $e) {
            return $this->json([
                'type' => 'https://symfony.com/errors/validation',
                'title' => 'Invalid input',
                'detail' => $e->getMessage(),
            ], 422);
        }

        $repository->deleteLikePath($path);

        // Get existing strings for URL path
        $newEntities = [];
        foreach ($parsedData as $locale => $messages) {
            foreach ($messages as $key => $data) {
                $entity = new TuiString($locale, $this->cleanPath($key), $data);
                $errors = $validator->validate($entity);
                if (count($errors)) {
                    return $this->json($errors);
                }
                $newEntities[] = $entity;
            }
        }
        $repository->bulkSave($newEntities);

        $tuiStringCachePool->invalidateTags($stringSerializer->getCacheKeys($path));

        $data = $stringSerializer->encodeEntities($newEntities);
        return $this->json($this->getOutputFormat($request) === 'i18n' ? $stringSerializer->toI18n($data) : $data, 201);
    }

    private function getOutputFormat(Request $request): string
    {
        $format = $request->query->get('format', self::DEFAULT_FORMAT);
        $format = in_array($format, ['i18n', 'flat']) ? $format : self::DEFAULT_FORMAT;

        return $format;
    }

    public function cleanPath(string $string): string
    {
        // Ensure bytestream is valid
        if (!mb_check_encoding($string, 'UTF-8')) {
            throw new \InvalidArgumentException('Invalid unicode input.');
        }

        // Clean and normalise unicode
        mb_substitute_character('none');
        $string = (string) mb_convert_encoding($string, 'utf-8', 'utf-8');
        $string = normalizer_normalize($string);

        // Strip control characters
        $string = (string) preg_replace('~\p{C}+~u', '', $string);

        // Strip invalid path characters and empty paths
        $string = (string) preg_replace('/[^[:alnum:_]\.]+/', '-', $string);
        $string = (string) preg_replace('/\.{2,}/', '.', $string);

        return $string;
    }
}
