<?php
namespace Tui\StringBundle;

use Symfony\Component\PropertyAccess\PropertyAccess;

class TuiStringSerializer
{
    private $propertyAccessor;

    public function __construct()
    {
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    public function encodeEntities(array $entities): array
    {
        $transformedEntities = array_reduce($entities, function ($map, $entity) {
            $locale = $entity->getLocale();
            if (!isset($map[$locale])) {
                $map[$locale] = [];
            }
            $map[$locale][$entity->getPath()] = $entity->getData();

            return $map;
        }, []);

        return $transformedEntities;
    }

    public function toI18n(array $locales): array
    {
        $output = [];

        foreach ($locales as $locale => $messages) {
            foreach ($messages as $key => $value) {
                $outputKey = join(array_map(function ($part) {
                    return '['.$part.']';
                }, array_merge([$locale], explode('.', $key))));
                $this->propertyAccessor->setValue($output, $outputKey, $value);
            }
        }

        return $output;
    }

    private function flattenRecursive(array $array, string $prefix = ''): array
    {
        $out = [];
        foreach ($array as $key => $value) {
            $innerKey = $prefix ? $prefix . '.' . $key : $key;
            if (is_array($value)) {
                $out = array_merge($out, $this->flattenRecursive($value, $innerKey));
                continue;
            }

            $out[$innerKey] =  $value;
        }
        return $out;
    }

    public function fromI18n(array $messages): array
    {
        $out = [];
        foreach ($messages as $locale => $messages) {
            $out[$locale] = $this->flattenRecursive($messages);
        }
        return $out;
    }

    public function getCacheKeys($path, int $maxDepth = null): array
    {
        if (is_array($path)) {
            return $this->getArrayCacheKeys($path, $maxDepth);
        }

        if (!is_string($path)) {
            throw new \InvalidArgumentException('First argument must be a string or array of strings');
        }

        $pathParts = explode('.', $path);
        if ($maxDepth) {
            $pathParts = array_slice($pathParts, 0, $maxDepth);
        }
        $cacheKeys = [];
        while (count($pathParts) > 0) {
            $cacheKeys[] = join('.', $pathParts);
            array_pop($pathParts);
        }
        return $cacheKeys;
    }

    private function getArrayCacheKeys(array $paths, int $maxDepth = null): array
    {
        $cacheKeys = array_reduce($paths, function ($cacheKeys, $path) use ($maxDepth) {
            return array_merge($cacheKeys, $this->getCacheKeys($path, $maxDepth));
        }, []);
        $cacheKeys = array_values(array_unique($cacheKeys));

        return $cacheKeys;
    }
}
