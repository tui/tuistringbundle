# TuiStringBundle

An API for managing strings compatible with vue-i18n

## Requirements

* Symfony 6.x or 7.x
* Doctrine ORM

## Installation

* Add & enable the bundle. This isn't (yet?) available on packagist, so you'll have to add our satis repository to your `composer.json`:

```json
{
    "type": "project",
    "repositories": [{
      "type": "vcs",
      "url": "https://bitbucket.org/tui/tuistringbundle.git"
    }],
    "require": {
      "…": "etc"
    }
}
```

Then:

```sh
composer require tuimedia/string-bundle
```

* Add the routes, e.g. create `config\routes\tui_string.yaml` (or edit your routes file, whatever):

```yaml
string_controllers:
    resource: "@TuiStringBundle/Controller/"
    type: annotation
    prefix: api
```

* Set up access control on your `security.yml`. You can dump the available routes with `bin/console debug:router | grep tui_string`. This is not done by the bundle because roles and permissions may vary between apps. Something like this is probably appropriate:

```yaml
    access_control:
        - { path: ^/api/strings, roles: [ROLE_ADMIN], methods: [POST, PUT, DELETE] }

```

* Update your database schema (or create and run a migration)

## Optional cache config

By default, the bundle uses the default app tag-aware cache configured by Symfony. You can change this by defining a cache pool named `tui_string_cache_pool` in `config/packages/cache.yaml`. Make sure tagging is enabled:

```yaml
framework:
    cache:
        pools:
            tui_string_cache_pool:
                adapter: cache.adapter.redis
                tags: true
```

## Using the Strings bundle with vue-i18n

In a typical vue-i18n project, you'll have one or more JSON locale files with a site's static content, for example `src/locales/en-GB.json`:

```json
{
    "app": {
        "loading": "Loading",
        "element": {
            "button": {
                "save": "Save"
            }
        }
    }
}
```

With vue-i18n set up, you set the user's locale and use the `$t` function to show text within a template:

```html
<div v-if="loading">{{ $t('app.loading') }}</div>
<form v-else>
    <button @click="save">{{ $t('app.element.button.save') }}</button>
</form>
```

This bundle provides a mechanism for overriding these static files, or replacing them entirely. A setup which loads all available locales into the frontend bundle, and then overrides the `app` strings from the Strings API might look like the following. Assuming your locale files exist in the `src/locales` folder of your frontend project, create `src/i18n.js` like this:

```js
import Vue from 'vue';
import VueI18n from 'vue-i18n';
import axios from 'axios';

// Load the plugin
Vue.use(VueI18n);

// Load static strings
function loadLocaleMessages() {
  const locales = require.context('./locales', true, /[A-Za-z0-9-_,\s]+\.json$/i);
  const messages = {};
  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i);
    if (matched && matched.length > 1) {
      const locale = matched[1];
      messages[locale] = locales(key);
    }
  });

  return messages;
}

// Fetch overrides and merge them into the static messages
function loadRemoteLocaleMessages(i18n) {
  axios.get('/api/strings/app').then(res => {
    Object.keys(res.data).forEach(locale => {
      i18n.mergeLocaleMessage(locale, res.data[locale]);
    });
  });
}

const i18n = new VueI18n({
  locale: process.env.VUE_APP_I18N_LOCALE || 'en-GB',
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'en-GB',
  messages: loadLocaleMessages(),
});

loadRemoteLocaleMessages(i18n);

export default i18n;
```

And in your `src/main.js`:

```js
import App from '@/App.vue';
import Vue from 'vue';
import i18n from '@/i18n';

Vue.config.productionTip = false;

new Vue({
  i18n,
  render: h => h(App),
}).$mount('#app');
```

## Strings API

See `openapi.json` for full API documentation

#### Validation

* The request body **must** be a JSON object. The property names must be valid locale names, with the property value being an object whose properties are either string data or nested objects following the same format.
* Providing data outside of the prefix given in the URL will return an error (e.g. supplying `app.loading` when the prefix is `app.element` will throw an error)
* All message paths (e.g. `app.home.title`) must be composed only of "word characters" (unaccented latin letters and numbers) and dot-separated.
* All locales must be provided for the given prefix, otherwise they'll be deleted.

### Quirks

* HTTP cache headers are set to private because we can't know whether the app is public (no login), private, or a mixture. Individual browsers will still cache these files, however.
* Key paths _can_ describe arrays, however unless these are strictly numerically indexed **in order** then `json_encode()` will describe them as a JSON object not an array. It's better to avoid arrays than to try to make them work.
