<?php

namespace Tui\StringBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Tui\StringBundle\Repository\TuiStringRepository;

#[ORM\Entity(repositoryClass: TuiStringRepository::class)]
#[ORM\UniqueConstraint(name: 'locale_path_unique', columns: ['locale', 'path'])]
#[ORM\Index(name: 'path_search_idx', columns: ['path'])]
#[UniqueEntity(fields: ['locale', 'path'], message: 'This path is already defined in this locale', errorPath: 'path')]
class TuiString
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 16)]
    #[Assert\Locale(canonicalize: true)]
    #[Assert\NotBlank]
    private ?string $locale;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    #[Assert\Type('string')]
    #[Assert\Regex('/^[\w .-]+$/', message: 'Message path must contain only numbers, letters, spaces, and hyphens')]
    private ?string $path;

    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    private ?string $data;

    #[ORM\Column(type: 'datetime')]
    private \DateTimeInterface $createdAt;

    public function __construct(string $locale, string $path, string $data)
    {
        $this->setLocale($locale);
        $this->setPath($path);
        $this->setData($data);
        $this->setCreatedAt(new \DateTime);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getData(): ?string
    {
        return $this->data;
    }

    public function setData(string $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }
}
