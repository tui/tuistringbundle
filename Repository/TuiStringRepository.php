<?php

namespace Tui\StringBundle\Repository;

use Tui\StringBundle\Entity\TuiString;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TuiString|null find($id, $lockMode = null, $lockVersion = null)
 * @method TuiString|null findOneBy(array $criteria, array $orderBy = null)
 * @method TuiString[]    findAll()
 * @method TuiString[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TuiStringRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TuiString::class);
    }

    public function findLikePath(string $path): array
    {
        return $this->createQueryBuilder('ld')
            ->select('ld')
            ->where('ld.path LIKE :path')
            ->setParameter('path', $path . '%')
            ->getQuery()
            ->getResult();
    }

    public function deleteLikePath(string $path)
    {
        return $this->createQueryBuilder('ld')
            ->delete()
            ->where('ld.path LIKE :path')
            ->setParameter('path', $path . '%')
            ->getQuery()
            ->getResult();
    }

    public function bulkSave(array $entities): void
    {
        foreach ($entities as $entity) {
            $this->getEntityManager()->persist($entity);
        }
        $this->getEntityManager()->flush();
    }
}
