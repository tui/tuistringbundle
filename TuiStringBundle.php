<?php
namespace Tui\StringBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TuiStringBundle extends Bundle
{
    const VERSION = 1.0;
}
